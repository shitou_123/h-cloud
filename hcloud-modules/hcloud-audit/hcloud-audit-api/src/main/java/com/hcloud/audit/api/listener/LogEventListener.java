package com.hcloud.audit.api.listener;

import com.hcloud.audit.api.bean.LoginLog;
import com.hcloud.audit.api.bean.OperateLog;
import com.hcloud.audit.api.event.LoginLogEvent;
import com.hcloud.audit.api.event.OperateLogEvent;
import com.hcloud.audit.api.feign.RemoteLogService;
import com.hcloud.common.core.base.HCloudResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Slf4j
@AllArgsConstructor
@Component
@EnableAsync
public class LogEventListener {
    private final RemoteLogService remoteLogService;

    @Async
    @EventListener(OperateLogEvent.class)
    public void listenOperateLog(OperateLogEvent event) {
        Object source = event.getSource();
        HCloudResult hepgResult = remoteLogService.saveLog((OperateLog) source);
        log.info("远程日志记录成功：{}", source);
    }

    @Async
    @EventListener(LoginLogEvent.class)
    public void listenLoginLog(LoginLogEvent event) {
        Object source = event.getSource();
        HCloudResult hepgResult = remoteLogService.saveLoginLog((LoginLog) source);
        log.info("远程日志记录成功：{}", source);
    }
}
