package com.hcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hepg
 * @date 2018年10月26日
 * 服务中心
 */
@SpringBootApplication
public class HCloudSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(HCloudSystemApplication.class, args);
    }

}
