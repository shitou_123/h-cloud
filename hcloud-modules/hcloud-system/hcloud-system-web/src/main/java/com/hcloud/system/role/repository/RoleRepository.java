package com.hcloud.system.role.repository;

import com.hcloud.system.role.entity.RoleEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @Auther hepangui
 * @Date 2018/11/5
 */
@Repository
public interface RoleRepository extends BaseRepository<RoleEntity> {
}
